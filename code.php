<?php

// While loop

function whileLoop(){
	$count = 5;

	while($count !== 0){
		echo $count. '<br/>';

		$count--;
	}
}

// do-while loop

function doWhileLoop(){
	$count = 20;

	do {
		echo $count. '<br/>';

		$count--;
	} while ($count > 0);
}

// for loop

function forLoop() {
	for ($count = 0; $count<= 20; $count++){
		echo $count. '<br/>';
	}
}


// Array manipulation

$studentNumbers = array('2020-1923', '2020-1924', '2020-1925');

$newStudentNumbers = ['2022-1111', '2022-1112', '2022-1113'];

// Simple arrays
	
	$tasks = [
		'drink HTML',
		'eat javascript',
		'inhale css',
		'bake sass'
	];

	// Associative Array
	$gradePeriods = [
		'firstGrading' => 98.5,
		'secondGrading' => 94.3
	];

	$heroes = [
		['Iron Man', 'Thor', 'Hulk'],
		['Wolverine', 'Cyclops', 'Jean', 'Grey' ],
		['Batman', 'Superman', 'Wonder Woman']
	];

	foreach ($heroes as $hero){
		echo $hero;
	}

	// Array sorting

	$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

	$sortedBrands = $computerBrands;
	$reverseSortedBrands = $computerBrands;

	sort($sortedBrands);
	rsort($reverseSortedBrands);


	// other Array functions

	function searchBrand($brands, $brand){
		return (in_array($brand, $brands)) ? "$brand is in the array." : "$brand is not in the array";
	}

$reverseGradesPeriods = array_reverse($gradePeriods);

// Activity 1
function activity1(){
	$count = 0;
	while($count <= 100){
		$iteration = $count +1;
		if ($count % 5 ===0){
			echo "This is the $count Iteration" ."<br/>";
		}

		$count ++;
	}
}

$students =[];

?>

