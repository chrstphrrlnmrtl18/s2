<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S2: Repitition Control Structures and Array Manipulation</title>
</head>
<body>
	<h1>Repitition Control Structures</h1>

	<?php whileLoop(); ?>

	<?php doWhileLoop(); ?>

	<?php forLoop(); ?>

	<?php echo $heroes[2][1]; ?>

	<?php echo $gradePeriods['secondGrading']; ?>

	<h2>Sorting</h2>
	<pre>
		<?php print_r($sortedBrands);?>

	</pre>

	<h2>Reverse Sorting</h2>
	
	<pre>
		<?php print_r($reverseSortedBrands);?>
	</pre>	

	<h2>Array Mutations (Append)</h2>

	<?php array_push($computerBrands, 'Apple'); ?>

	<?php array_unshift($computerBrands, 'Dell'); ?>

	<pre>
		<?php print_r($computerBrands);?>
	</pre>

	<h2>Remove</h2>
	<?php array_pop($computerBrands); ?>
	<pre>
		<?php print_r($computerBrands);?>
	</pre>

	<?php array_shift($computerBrands); ?>
	<pre>
		<?php print_r($computerBrands);?>
	</pre>

	<h2>Count</h2>

	<pre>
		<?php echo count($computerBrands);?>
	</pre>



	<pre>
		<?php echo searchBrand($computerBrands, 'Asus'); ?>
	</pre>

	<pre>
		<?php print_r($reverseSortedBrands); ?>
	</pre>

	<pre>
		<?php activity1(); ?>
	</pre>

	<?php array_push($students, 'Chris');?>
	<pre>
		<?php print_r($students); ?>
	</pre>

	<pre>
		<?php echo count($students);?>
	</pre>

	</pre>

	<?php array_push($students, 'Topher');?>
	<pre>
	<pre>
		<?php print_r($students); ?>
	</pre>
	<pre>
		<?php echo count($students);?>
	</pre>

	</pre>

	<?php array_shift($students); ?>
	<pre>
	<pre>
		<?php print_r($students); ?>
	</pre>
		<pre>
		<?php echo count($students);?>
	</pre>





</body>
</html>